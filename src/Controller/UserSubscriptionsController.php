<?php

namespace Drupal\stripe_registration\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\stripe_registration\StripeRegistrationService;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class UserSubscriptionsController.
 *
 * @package Drupal\stripe_registration\Controller
 */
class UserSubscriptionsController extends ControllerBase {

  /**
   * Drupal\stripe_registration\StripeRegistrationService definition.
   *
   * @var \Drupal\stripe_registration\StripeRegistrationService
   */
  protected $stripeApi;
  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(StripeRegistrationService $stripe_api, EntityTypeManagerInterface $entityTypeManager) {
    $this->stripeApi = $stripe_api;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('stripe_registration.stripe_api'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * SubscribeForm.
   *
   * @return array
   *   Return SubscribeForm.
   */
  public function subscribeForm() {
    $form = $this->formBuilder()->getForm('Drupal\stripe_registration\Form\StripeSubscribeForm');

    return $form;
  }

  /**
   * Cancel subscription.
   */
  public function cancelSubscription() {

    $remote_id = \Drupal::request()->get('remote_id');

    try {
      $this->stripeApi->cancelRemoteSubscription($remote_id);
      $local_subscription = $this->stripeApi->loadLocalSubscription(['subscription_id' => $remote_id]);
      // Setup the status of a local subscription in 'cancel'.
      // As the Stripes changes the status after some time wee will not to
      // synchronize local subscription with a remote subscription.
      $local_subscription->set('status', 'canceled');
      // Roles related with a subscription will be removed after calling 'save()'
      // method, because saving initiates a call of the 'updateUserRoles()'
      // method of a local subscription entity.
      $local_subscription->save();
    }
    catch (\Exception $e) {
    }

    // Attempt to redirect a user back to a view.
    // To redirect a user back to a view the "Include destination" option
    // should be checked for the "Operations links" view's field.
    // If a 'destination' argument is not correct, then just display a message.
    $destination = Url::fromUserInput($this->getDestinationArray()['destination']);
    if ($destination->isRouted()) {
      return $this->redirect($destination->getRouteName());
    }
    else {
      return [
        '#markup' => $this->t('The subscription was canceled.'),
      ];
    }
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   TRUE if the user is allowed to cancel the subscription.
   */
  public function accessCancelSubscription(AccountInterface $account) {
    $remote_id = \Drupal::request()->get('remote_id');

    return AccessResult::allowedIf($account->hasPermission('administer stripe subscriptions') ||
      ($account->hasPermission('manage own stripe subscriptions') && $this->stripeApi->userHasStripeSubscription($account, $remote_id)));
  }

  /**
   * Reactivate subscription.
   */
  public function reactivateSubscription() {
    $remote_id = \Drupal::request()->get('remote_id');

    $this->stripeApi->reactivateRemoteSubscription($remote_id);
    $this->stripeApi->syncRemoteSubscriptionToLocal($remote_id);

    return $this->redirect("stripe_registration.user.subscriptions.viewall", [
      'user' => $this->currentUser()->id(),
    ]);
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   TRUE if the user is allowed to reactivate a subscription.
   */
  public function accessReactivateSubscription(AccountInterface $account) {
    $remote_id = \Drupal::request()->get('remote_id');

    return AccessResult::allowedIf($account->hasPermission('administer stripe subscriptions') ||
      ($account->hasPermission('manage own stripe subscriptions') && $this->stripeApi->userHasStripeSubscription($account, $remote_id)));
  }

}
